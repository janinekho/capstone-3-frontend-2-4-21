import UserContext from '../../UserContext';
import React,{ useContext, useState , useEffect} from 'react';
import { Button, Card, Form } from 'react-bootstrap';
import Link from 'next/link';
import Calculator4 from '../../components/Calculator4';
import transactionData from  '../../data/transactions';
import Router from 'next/router';
import AppHelper from '../../app-helper';
import MyApp from '../_app.js';
import moment from 'moment';
/*import '../styles/globals.css';*/


export default function tracker(){
      

   	const{ user, setUser } = useContext(UserContext);
		const [tname, setTName]= useState('');
		const[email,setEmail]= useState('');
    const [budget,setBudget]= useState(0);
    const[transactionType, setTransactionType] = useState('');
    const[amount, setAmount]= useState('');
   const [remarks, setRemarks] = useState('');
   const[box, setBox]= useState('');
   
   const[description, setDescription]= useState('');
   const[category,setCategory]= useState([]);
   const[id, setId] = useState(0); 
   let [total, setTotal]= useState(0);  


  console.log(total)

  
  /*let [total, setTotal]= useState(0);*/
    /*const transaction = transactionData.map(indivTransaction =>{
      total += indivTransaction.amount;
    })*/

    
    
   useEffect(()=>{
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/category`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
        
        })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        
       setCategory(data);
       
        
      })

  },[])
   
   
    
    
    function authenticate(e){
      
      
         console.log(category);
         let type = 0;
         let _id = 0;
         let des =0;
         let Len = category.length;
         for (var i = 0; i < Len; i++) {
            if(description == category[i].description){
              type =(category[i].type);
              _id=(category[i]._id);

              console.log(category[i].type)
              console.log(category[i]._id)
              console.log(category[i].description)   
            }

            
         }

      let tempAmount = amount;
     if(type == "Expense"){
      tempAmount = tempAmount * -1
     }

     

    const options = {
        method: 'POST',
        headers: {Authorization : `Bearer ${AppHelper.getAccessToken()}`, 'Content-Type': 'application/json'}, 
        body: JSON.stringify({
          transactionType: type,
          transactionDescription: description,
          categoryId: _id,
           amount: tempAmount,
           remarks: remarks
        })
       }
      console.log(options);
    fetch(`${AppHelper.NEXT_PUBLIC_API_URLL}/users/post-transaction`, options)
    .then (AppHelper.toJSON)
      .then(transactionData => {
        console.log(transactionData);
        setDescription(description)
          
         
          })
      }  
  

    
    const boxOnClick=(e)=>{
      category.map((type) => {
      if(type.type == 'Income' ){
        console.log(yell)
        setBox(true);
       }
    })
  } 

  useEffect(()=>{
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transaction-details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        
        })
      .then(res => res.json())
      .then(data => {
        
     

        console.log(data);
         

         let totalAmount = 0;
        let Len = data.length;
         for (var i = 0; i < Len; i++) {
          
         setTotal(totalAmount += data[i].amount)
         

           
        }

      })

  }, [])
    	return (
      <React.Fragment>
        <div className="expensetracker">
        <div className="profile">
        <h2> Expense Tracker </h2>
          <Card class="col-md-3" controlid="trackerProfile">
          <Card.Header class="card header"> Profile 
          <Link href="/editProfile">
                <a><button> Edit </button></a>
                </Link>
          </Card.Header>

            <Card.Body>
               Full Name: {user.firstName} {user.lastName} <br/>
               {/*Initial Budget: {user.budget}*/}

                        
            </Card.Body>
          </Card>       
        <br />
        
          <h4 >History
          <Link href="/history">
          <a><button> >>> </button></a>
          </Link>
        </h4>
         
       
        <br />
         <Card Card class="col-md-4">  
        <h3 > Available Balance: {total} </h3>
            </Card>
        </div>

      <div className="transaction">
        <div className="post-transaction">

        <h2 > Post Your Transaction </h2>


          <Form onSubmit={e => authenticate(e)}>
                 
                <Form.Group controlId="transactionType">
                  
                  <Form.Label> Select Transaction </Form.Label>
                    
                    <Form.Control as="select" value={description}  onChange= {e => {setDescription(e.target.value)
                      console.log(description)}}>
                       

                      
                      <option value="" > - </option>
                      {category.map((category) => {
                        return(                        
                        <React.Fragment>
                        <option value={category.description}>{category.description}</option>
                         <h1 value={transactionType}>Transaction Type: {category.type}  </h1>  
                      </React.Fragment>                                
                       )

                       })}
                       
                     
                     
                    </Form.Control>
                   

                </Form.Group>
                     
                

                  <Form.Group controlId="transactionAmount">
                    <Form.Label> Transaction Amount </Form.Label>

                          <Form.Control
                        placeholder="Enter Amount"
                        type= "number"
                        value={amount}
                        onChange={e => setAmount(e.target.value)}
                        />
                          
                        
                        
                       
                  </Form.Group>   

                  <Form.Group controlId="transactionRemarks">
                    <Form.Label> Transaction Remarks </Form.Label>
                      <Form.Control as="textarea"
                                    value={remarks}
                                    onChange={e => setRemarks(e.target.value)}
                        />
                  </Form.Group> 
                <button> Submit </button> 
          </Form>

        </div>
         <div className="Calculator">
        <h2> </h2>
          <div> 
          <Calculator4 />
          </div>
          </div>
          </div>
          </div>
      </React.Fragment>

    )
     
}
