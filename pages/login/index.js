import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col,Card } from 'react-bootstrap'
import {GoogleLogin} from 'react-google-login';
import Swal from 'sweetalert2';
import Head from 'next/head';
import Router from 'next/router';
import UserContext from '../../UserContext';
import Users from '../../data/users';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import Image from 'next/image'


export default function Login() {

                // Use the UserContext and destructure it to access the user and setUser defined in the App component
  const { setUser } = useContext(UserContext)

                // States for form input
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(true);
  

function authenticate(e) {
                    // Prevent redirection via form submission
  e.preventDefault()
      
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
       },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
      

// Successful authentication will return a JWT via the response accessToken property
    if(data.accessToken){

// Store JWT in local storage
      localStorage.setItem('token', data.accessToken);

// Send a fetch request to decode JWT and obtain user ID and role for storing in context
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${data.accessToken}`
      } 
    })
      .then(res => res.json())
      .then(data => {

// Set the global user state to have properties containing authenticated user's ID
      setUser({
        id: data._id
      })
      Router.push('/')

      })

    }else{

      if(data.error === 'does-not-exist'){
        Swal.fire('Authentication Failed', 'User does not  exist.', 'error')
      } else if (data.error === 'login-type-error'){
        Swal.fire('Login Type Error', 'You may have registered to a different login procedure, try an alternative login procedure', 'error')
      } else if (data.error === 'incorrect-password'){
        Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
      }
    }
                        
  })
}
    
   const authenticateGoogleToken = (response) =>{
      console.log(response);
      const payload = {
        method: 'POST',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({tokenId:response.tokenId})
      }


   fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`,payload)
      .then(AppHelper.toJSON)
      .then(data => {
         console.log(data);
         if(typeof data.accessToken !== 'undefined'){
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
           Router.push('/')
         }else{
          if(data.error === 'google-auth-error'){
            Swal.fire(
              'Google Auth Error',
              'Google authentication procedure failed.',
              'error'
              )

          } else if(data.error === 'login-type-error'){
            Swal.fire(
              'Login Type Error',
              'You may have registered through a different login procedure.',
              'error'
              )
            }


        }
      })


   }
  
   const retrieveUserDetails = (accessToken) => {
    const options ={
      headers: {Authorization: `Bearer ${accessToken}`}
    };
    //localhost:4000/api/users/details
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, options)//since napasa na natin yung headers sa taas tatawagin nalang options
      .then(res => res.json())
      .then(data=>{
        console.log(data);
        //get from setUser above para machange value

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email
        });
        Router.push('/')

   })
  }    
    
      

  useEffect(() => {
    if((email !== '' && password !== '')){
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [ email, password ])


      return (
        <React.Fragment>
        <div className="login-container-1">
        
      
      <Card className="loginBox">

          <Card.Header> Login Details </Card.Header>
          <Card.Body>
             <Form onSubmit={e => authenticate(e)}>
             <div className="fields">
        <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email}
              onChange={(e) => setEmail(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password}
              onChange={(e) => setPassword(e.target.value)}
                required
            />
        </Form.Group>

        { isActive
        ? 
        <Button className="submitBtn" type="submit">
          Submit
        </Button>
        :
        <Button className="bg-danger" type="submit" disabled>
          Submit
        </Button>

      }




      <GoogleLogin 
             clientId= "543967147292-88s4c4mv95m2ihmem08b9lqfkiasg98p.apps.googleusercontent.com"
             buttonText="Login"
             onSuccess={ authenticateGoogleToken }
             onFailure={ authenticateGoogleToken }
             cookiePolicy ={ 'single_host_origin' }
             className="w-100 text-center d-flex justify-content-center"
      />


       </div>
        </Form>

          </Card.Body>
      </Card> 
      <img className="plantpic" src="/plant.jpg" alt="plant" /> 
       </div>
      
      </React.Fragment>

      )

}

