import React,{useState,useEffect} from 'react';
import { Table, Button,Col, Form} from 'react-bootstrap';
import Link from 'next/link';
import transactionData from  '../../data/transactions';
/*import Users from '../data/users';*/
import { MDBCol, MDBIcon } from "mdbreact";
import moment from 'moment';
import AppHelper from '../../app-helper';

export default function history() {
  const [ record, setRecord] = useState([]);
  const[date, setDate] = useState('');
  const[type, setType]= useState('');
  const [amount, setAmount] = useState(0);
  let [total, setTotal]= useState(0);
  const[description, setDescription]= useState('');
  const [searchTerm, setSearchTerm] = useState([]);
  const [search, setSearch] = useState([]);
  const [transactionId, settransactionId] = useState('');
  const[filteredSearch, setfilteredSearch] = useState([])
   

  useEffect(()=>{     
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transaction-details`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        
        })
      .then(res => res.json())
      .then(data => {
        setSearchTerm(data)
     

        
         setDate(data.date);
         setType(data.transactionType)
         setDescription(data.transactionDescription)
         

         let totalAmount = 0;
        let Len = data.length;
         for (let i = 0; i < Len; i++) {
          
         setTotal(totalAmount += data[i].amount)
         setType(data[i].transactionType)
         setDescription(data[i].transactionDescription)
          console.log(data[i]._id)
           
        }
          
         /* if (data._id){
          setRecord(data);

        } else {
          setRecord([]);
        }*/
        setRecord(data)
        console.log(data);
      
      

       
        })

  }, [])

  console.log(transactionId)
   
   useEffect(() =>{
   setRecord(
   search.length === 0 ? searchTerm
: searchTerm.filter(term =>                        
 term.transactionType.toLowerCase().includes(search.toLowerCase()))
 )
console.log(filteredSearch)
 },[search])


  function erase(e){
   
    console.log(e)
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${e}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization : `Bearer ${ AppHelper.getAccessToken()}`    
        }
        
        })
      .then(res => res.json())
      .then(data => {       
        console.log(data);
         
      })
     }
  


/*function handleChange(){

   
   for (var i =0; i< record.length; i++){
    console.log(record[i].description)
  const filter = record.length === 0 ? record
: record.filter(record =>                         
 record[i].description.toLowerCase().includes(search.toLowerCase()))

  }
}*/
 /*
function handleChange(){
   for (var i =0; i< record.length; i++){
   
    if (record[i].description == e.target.value){
      console.log(yellow)
    }
  var startDate = new Date("2021-01-24");
        var endDate = new Date("2021-01-25");
        var filterData = record.filter(a => {
          var date = new Date(a.date);
          return (record.date>= startDate && date <= endDate);
        });
      if(!element.find(make => make === element.make)){
           makes.push(element.make);
        }

        
   }

}*/

 /*const handleChange = event => {
    
    const results = record.filter(record =>
      record.toLowerCase().includes(record)
    )
    

  };*/

  /*useEffect(() => {
    const results = record.filter(record =>
      record.toLowerCase().includes(record)
    );
    setRecord(results);
  }, []);*/

 
  return(

    <React.Fragment>
    <div className="search-bar">
      <div className="input-group md-form form-sm form-1 pl-0">
      <a className="search-btn" href="#"> </a>
        <div className="input-group-prepend">
          <span className="input-group-text purple lighten-3" id="basic-text1">
            <img className="search" src="/search.png" alt="search" width="25px" height="25px" />         
          </span>
        </div>                   
         <input className="search-txt"
          type="text"
          className="form-control my-0 py-1"
          id="addInput"
          placeholder="Search/Type Description Here"

          value={search} onChange= {e => {setSearch(e.target.value)
                      console.log(search)}}/>                
        </div>
     </div>

      <div>
      <h2>Total:{total}</h2>
     </div> 
          
     <Table>
        <thead>
        <tr>
         <th> Date</th>
         <th> Transaction Type </th>
         <th> Transaction Description </th>
         <th> Amount</th>
         <th> Remarks </th>
        </tr>
        </thead>
         
      
      <tbody>
     {record.map((record) => {
     
      return(
          
          <tr>
               
                <td>
                <button className="deletebutton" value={record._id} onClick={e => {erase(e.target.value)}}> Delete </button>
                {moment(record.date).format('MMMM DD YYYY')}
                </td>

                <td>
                  {record.transactionType}
                </td>

               <td>{record.transactionDescription}</td>
               <td>{record.amount}</td>
               <td>{record.remarks}</td>
               
            </tr>
            

         )

     })}

        </tbody>

     
     </Table>
     <Link href="/tracker">
      <button> Back to the Tracker </button>
    </Link>
     </React.Fragment>


    
    

    )
    
}