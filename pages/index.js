import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import React,{useContext, useState, useEffect} from 'react';
import { Jumbotron, Button, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

/*import Users from '../data/users';*/
import Link from 'next/link';
import Router from 'next/router';
import AppHelper from '../app-helper';



export default function Hello({userData}){
	const{ user, setUser } = useContext(UserContext);
    const [email, setEmail]= useState('');
		const [name, setName]= useState('');
    const[registeredUser, setRegisteredUser]= useState(true);

    
        
   

    
   useEffect(() =>{

    
    const options = {
     headers: {Authorization : `Bearer ${AppHelper.getAccessToken()}`}
    }

    fetch(`${AppHelper.API_URL}/users/details`, options)
      .then (AppHelper.toJSON)
      .then(userData => {
        if (typeof userData._id !== 'undefined'){
          setUser({
            id: userData._id,
            isAdmin: userData.isAdmin,
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.email

          })
        
         console.log(userData)
         console.log(user)
         setEmail(userData.email)
         
        } else {
          setUser({ 
            id:null, 
            isAdmin:null,
            firstName: null,
            lastName: null,
            email:null
          })
          setEmail('')
        }

      })

    },[user.email])

 console.log(email)
console.log(email != '')
  
 
   
 
	return( 
    <React.Fragment>
	  <div className="banner">
          <div className="bannerClass">
          <h1> Hello There, {user.firstName} {user.lastName}</h1>
          <p> Time to get a hold of your budget </p>
          <img src="/plant-hand.png" width="300" height="300"/>
          {email != ''
            ?

          <Link href="/tracker">   
          <a controlid="btn btn1"><button>Start Now -></button></a>
          </Link>
          :
          <Link href="/login">
          <a controlid="btn btn2"> <button>Start Now -></button> </a>

          </Link>
           }
          </div>
          

      </div>
      <footer>
      </footer>

      
     </React.Fragment>
      )

    /* })*/
}


/**/
    
   		
 /*export async function getServerSideProps() {
  const res = await fetch('http://localhost:4000/api/')
  const data = await res.json()

  fetch('http://localhost:4000/api/courses')
  .then(res => res.json())
  .then(data)

  return {
    props: {
      data
    }

  }
  }     */