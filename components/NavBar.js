import React,{ useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar(){//function name NavBar should match the file name NavBar.js
    /*why capital N. kasi we're  grabbing yung import Navbar sa taas line 2*/
    const { user } = useContext(UserContext);
    return (
        <Navbar className="navbar" bg="#adce74" expand="lg"> 
                    <Link href= "/">
                    <a className="navTitle">Good Steward</a>
                    </Link>
                
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            
                            
                            <Link href="/">
                            <a className ="nav-link" id="button-1" role= "button"> Home </a>
                            </Link>

                
                    
                             {(user.email !== null)
                                ? 
                                <React.Fragment>
                                <Link href="/dashboard">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> Dashboard/Reports </a>
                                    </Link>
                                <Link href="/categories">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> Add Categories </a>
                                    </Link>
                                <Link href="/tracker">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> Budget Tracker </a>
                                    </Link>
                                <Link href="/history">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> <b>HIS</b>tory </a>
                                    </Link>

                                <Link href="/logout">
                                    <a className ="nav-link" id="button-1" role= "button"> Logout </a>
                                    </Link>
                              </React.Fragment>      
                            : 
                            <React.Fragment>
                                <Link href="/login">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> Login </a>
                                </Link>

                                <Link href="/register">
                                    <a className ="nav-link" id="button-1" role= "button" data-toggle="collapse"> Register </a>
                                </Link>
                            </React.Fragment>
                            }
                            
                           
                        </Nav>
                    </Navbar.Collapse>

                </Navbar>
        

        )
}

