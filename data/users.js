const Users = [
            {  name: "Admin",
                email: "admin@mail.com",
                password: "admin123",
                isAdmin: true
            },
            {
                name: "Jano Gibs",
                email: "jan@mail.com",
                password: "jan1234",
                isAdmin: false,
                budget: [
                {
                
                    date:   "12/1/2020",
                    amount: 3000,
                    transactionType: "income"
                 },
                 {
                    date: "12/2/2020",
                    amount: -1000,
                    transactionType: "donation"
                }
               

              ] 
            }
           
        ]

        export default Users